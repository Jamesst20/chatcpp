#include "ClientServer.h"

ClientServer::ClientServer()
{
    this->expectedDataSize = 0;
    this->socket = new QTcpSocket();

    connect(this->socket, SIGNAL(connected()), this, SIGNAL(connectedToServer()));
    connect(this->socket, SIGNAL(disconnected()), this, SIGNAL(disconnectedFromServer()));
    connect(this->socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(this->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));


}

void ClientServer::connectToHost(QString IP, qint16 port){
    this->socket->connectToHost(IP, port);
}

void ClientServer::disconnect(){
    this->socket->disconnectFromHost();
}

void ClientServer::readyRead(){
    QDataStream in(socket);

    if (this->expectedDataSize == 0)
    {
        if (socket->bytesAvailable() < (int)sizeof(quint16)){
            return;
        }
        in >> this->expectedDataSize;
    }

    if (this->socket->bytesAvailable() < this->expectedDataSize){
        return;
    }

    QString data;
    in >> data;

    this->expectedDataSize = 0;

    emit commandReceived(data);
    readyRead();
}

void ClientServer::sendCommand(Command *command){
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);

    //Leave space for paquet size
    out << (quint16) 0;
    //Append command
    out << command->getFormattedCommand();
    //Move cursor back to index 0.
    out.device()->seek(0);
    //Write size
    out << (quint16) (paquet.size() - sizeof(quint16));
    //Send packet
    this->socket->write(paquet);
}

void ClientServer::error(QAbstractSocket::SocketError){
    emit serverError(this->socket->errorString());
}
