#include "GUI_Chat.h"
#include "ui_GUI_Chat.h"

GUI_Chat::GUI_Chat(ClientServer *clientServer, ClientController *clientController) : ui(new Ui::GUI_Chat)
{
    ui->setupUi(this);
    this->clientServer = clientServer;
    this->clientController = clientController;

    connect(this->clientController, SIGNAL(messageReceived(QString, QString)), this, SLOT(messageReceived(QString, QString)));
    connect(this->clientController, SIGNAL(usersListReceived(QStringList)), this, SLOT(usersListReceived(QStringList)));
    connect(this->clientController, SIGNAL(newUserConnected(QString)), this, SLOT(newUserConnected(QString)));
    connect(this->clientController, SIGNAL(userDisconnected(QString)), this, SLOT(userDisconnected(QString)));

    this->clientServer->sendCommand(new CommandGUsersList());
    this->setWindowTitle(this->clientController->getUsername());

    //Add filter for on enter key press event
    ui->txt_message->installEventFilter(this);
}

GUI_Chat::~GUI_Chat()
{
    delete ui;
}

bool GUI_Chat::eventFilter(QObject *object, QEvent *event){
    //Send message when Enter key is pressed on txt_message
    if (object == ui->txt_message && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->key() == Qt::Key_Return)
        {
            on_bt_send_clicked();
            return true;
        }
    }
    return QMainWindow::eventFilter(object, event);
}

void GUI_Chat::usersListReceived(QStringList users){
    ui->list_users_online->clear();
    for(int i = 0; i < users.size(); i++){
        ui->list_users_online->addItem(users.at(i));
    }
    ui->list_users_online->sortItems();
}

void GUI_Chat::messageReceived(QString from, QString message){
    if(from.length() > 0 ){
        this->ui->txt_chat_messages->append("<b>" + from + "</b>" + " : " + message);
    }else{
        this->ui->txt_chat_messages->append(message);
    }
}

void GUI_Chat::newUserConnected(QString username){
    ui->list_users_online->addItem(username);
    ui->list_users_online->sortItems();
    ui->txt_chat_messages->append("### <b>" + username + "</b> has connected. ###");
}

void GUI_Chat::userDisconnected(QString username){
    for(int i = 0; i < ui->list_users_online->count(); i++){
        if(ui->list_users_online->item(i)->text() == username){
            delete ui->list_users_online->takeItem(i);
            ui->txt_chat_messages->append("### <b>" + username + "</b> has disconnected. ###");
            break;
        }
    }
}

void GUI_Chat::on_action_disconnect_triggered()
{
    this->clientServer->disconnect();
}

void GUI_Chat::on_bt_send_clicked()
{
    if(ui->txt_message->toPlainText().length() > 0){
        this->clientServer->sendCommand(new CommandMessage(this->clientController->getUsername(), ui->txt_message->toPlainText()));
        this->ui->txt_message->clear();
    }
}
