#include "DialogServerList.h"
#include "ui_DialogServerList.h"


DialogServerList::DialogServerList(QWidget *parent) : QDialog(parent), ui(new Ui::DialogServerList)
{
    ui->setupUi(this);

    this->setWindowTitle("Servers");

    futureWatcher = new QFutureWatcher<void>();
    connect(futureWatcher, SIGNAL(finished()), this, SLOT(getServerListTaskFinished()));
    //Run task
    futureWatcher->setFuture(QtConcurrent::run(std::mem_fun(fetchServersList), this));

}

void DialogServerList::fetchServersList(){
    this->serverList = Utils::getServerList();
}

void DialogServerList::getServerListTaskFinished(){
    QStringList IPs = this->serverList.split("\n");

    ui->list_server_ips->clear();
    ui->list_server_ips->setRowCount(0);

    QStringList headers;
    headers << "IP Externe" << "IP Local" << "Port" << "PC Name";
    ui->list_server_ips->setHorizontalHeaderLabels(headers);
    for(int row = 0; row < IPs.size(); row++){
        QStringList splittedIP = IPs.at(row).split(";");
        if(splittedIP.length() == 4){
            ui->list_server_ips->setRowCount(ui->list_server_ips->rowCount() + 1);
            for(int column = 0; column < 4; column++){
                ui->list_server_ips->setItem(row, column, new QTableWidgetItem(splittedIP.at(column)));
            }
        }
    }

}

DialogServerList::~DialogServerList()
{
    delete ui;
    delete futureWatcher;
}

QString DialogServerList::showAndWait(){
    this->setModal(true);
    //Block until closed
    QDialog::exec();
    //Return selected item
    if(ui->list_server_ips->selectedItems().size() > 0 && ui->list_server_ips->selectedItems().at(0)){
        return ui->list_server_ips->selectedItems().at(0)->text();
    }
    return "";
}

void DialogServerList::on_bt_select_server_clicked()
{
    this->close();
}

void DialogServerList::on_list_server_ips_itemSelectionChanged()
{
    if(ui->list_server_ips->selectedItems().size() > 0 && ui->list_server_ips->selectedItems().at(0)){
        if(ui->list_server_ips->selectedItems().at(0)->column() >= 2){
            ui->list_server_ips->selectedItems().at(0)->setSelected(false);
        }
    }
}
