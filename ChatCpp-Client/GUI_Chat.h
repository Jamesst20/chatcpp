#ifndef GUI_CHAT_H
#define GUI_CHAT_H

#include <QMainWindow>
#include <QStringList>
#include <QKeyEvent>

#include "ClientServer.h"
#include "ClientController.h"
#include "CommandMessage.hpp"

namespace Ui {
class GUI_Chat;
}

class GUI_Chat : public QMainWindow
{
    Q_OBJECT

public:
    explicit GUI_Chat(ClientServer *clientServer, ClientController *clientController);
    ~GUI_Chat();

private slots:
    void on_action_disconnect_triggered();
    void on_bt_send_clicked();
    void messageReceived(QString from, QString message);
    void usersListReceived(QStringList);
    void newUserConnected(QString username);
    void userDisconnected(QString username);    

private:
    virtual bool eventFilter(QObject *object, QEvent *event);

    Ui::GUI_Chat *ui;
    ClientServer *clientServer;
    ClientController *clientController;
};

#endif // GUI_CHAT_H
