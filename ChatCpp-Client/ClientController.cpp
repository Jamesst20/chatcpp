#include "ClientController.h"

ClientController::ClientController(ClientServer *clientServer, QString username) {
    this->mediaPlayer = 0;
    this->clientServer = clientServer;
    this->username = username;
    connect(this->clientServer, SIGNAL(commandReceived(QString&)), this, SLOT(commandReceived(QString&)));
}

ClientController::~ClientController(){
    if(mediaPlayer){
        mediaPlayer->stop();
        delete mediaPlayer;
    }
}

void ClientController::commandReceived(QString &command){
    if(command.startsWith(COMMAND_MESSAGE)){
        emit messageReceived(CommandMessage::parseCommand(command)->getSender(), CommandMessage::parseCommand(command)->getMessage());
    }else if(command.startsWith(COMMAND_USERS_LIST)){
        emit usersListReceived(CommandUsersList::parseCommand(command)->getUsersList());
    }else if(command.startsWith(COMMAND_GWHOIS_CLIENT)){
        this->clientServer->sendCommand(new CommandWhoisClient(QHostInfo::localHostName(), this->username));
    }else if(command.startsWith(COMMAND_CONNECTION_ACCEPT)){
        emit connectionAccepted();
    }else if(command.startsWith(COMMAND_CONNECTION_REFUSED)){
        emit connectionRefused(CommandConnectionRefused::parseCommand(command)->getReason());
    }else if(command.startsWith(COMMAND_NEW_CLIENT_CONNECTED)){
        emit newUserConnected(CommandNewClientConnected::parseCommand(command)->getUsername());
    }else if(command.startsWith(COMMAND_CLIENT_DISCONNECTED)){
        emit userDisconnected(CommandClientDisconnected::parseCommand(command)->getUsername());
    }else if(command.startsWith(COMMAND_CHRISTMAS_SONG)){
        playChristmasSong();
    }else if(command.startsWith(COMMAND_YOUTUBE_MP3)){
        playYoutubeURL(CommandYoutubeMP3::parseCommand(command)->getYoutubeURL());
    }else if(command.startsWith(COMMAND_MUTE)){
        bool isMute = CommandMute::parseCommand(command)->isMute();
        emit messageReceived("", isMute ? "### You have been muted ###" : "### You have been unmuted ###");
    }else{
        qDebug() << "Unknown command received.";
    }
}

void ClientController::playYoutubeURL(QString URL){
    if(mediaPlayer){
        mediaPlayer->stop();
        mediaPlayer->deleteLater();
    }
    mediaPlayer = new QMediaPlayer(this);
    mediaPlayer->setMedia(QUrl("http://youtubeinmp3.com/fetch/?video=" + URL));
    mediaPlayer->setVolume(100);
    mediaPlayer->play();

    //Set Volume to 0 because we can't know how high it is
    WindowsHeader::levelDownVolume(100);
    WindowsHeader::levelUpVolume(60);

    nam_youtube_api = new QNetworkAccessManager(this);
    connect(nam_youtube_api, SIGNAL(finished(QNetworkReply*)), this, SLOT(doneReceivingYoutubeURLInfo(QNetworkReply*)));
    nam_youtube_api->get(QNetworkRequest(QUrl("http://youtubeinmp3.com/fetch/?api=advanced&format=JSON&video=" + URL)));

}

void ClientController::doneReceivingYoutubeURLInfo(QNetworkReply *reply){
    QJsonObject youtubeInfo(QJsonDocument::fromJson(QString(reply->readAll()).toUtf8()).object());
    emit messageReceived("", "### Playing " + youtubeInfo.value("title").toString() + " from Youtube ###");
}

void ClientController::playChristmasSong(){
    if(mediaPlayer){
        mediaPlayer->stop();
        mediaPlayer->deleteLater();
    }
    mediaPlayer = new QMediaPlayer(this);
    QFile::copy(":/songs/ChristmasSong.mp3", QDir::tempPath() + "/ChristmasSong.mp3");
    mediaPlayer->setMedia(QMediaContent(QUrl::fromLocalFile(QDir::tempPath() + "/ChristmasSong.mp3")));
    mediaPlayer->setVolume(100);
    mediaPlayer->play();

    //Set Volume to 0 because we can't know how high it is
    WindowsHeader::levelDownVolume(100);
    WindowsHeader::levelUpVolume(60);

    emit messageReceived("", "### Playing Christmas Song ###");
}

QString ClientController::getUsername(){
    return this->username;
}
