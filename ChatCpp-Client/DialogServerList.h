#ifndef DIALOGSERVERLIST_H
#define DIALOGSERVERLIST_H

#include <QDialog>
#include <QTableWidgetItem>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>
#include <QStringList>


#include "Utils.h"

namespace Ui {
class DialogServerList;
}

class DialogServerList : public QDialog
{
    Q_OBJECT

public:
    explicit DialogServerList(QWidget *parent = 0);
    ~DialogServerList();

    QString showAndWait();

private slots:
    void getServerListTaskFinished();
    void on_bt_select_server_clicked();
    void on_list_server_ips_itemSelectionChanged();

private:
    Ui::DialogServerList *ui;
    QString serverList;
    QFutureWatcher<void> *futureWatcher;

    void fetchServersList();
};

#endif // DIALOGSERVERLIST_H
