QT += core
QT += widgets
QT += network
QT += multimedia

TEMPLATE = app

SOURCES += \
    Main.cpp \
    GUI_Login.cpp \
    GUI_Chat.cpp \
    ClientServer.cpp \
    ClientController.cpp \
    DialogServerList.cpp

FORMS += \
    GUI_Login.ui \
    GUI_Chat.ui \
    DialogServerList.ui

HEADERS += \
    GUI_Login.h \
    GUI_Chat.h \
    ClientServer.h \
    ClientController.h \
    WindowsHeader.h \
    DialogServerList.h \
    Utils.h

RESOURCES += \
    resources.qrc

INCLUDEPATH += $$PWD/../ChatCpp-Commands
DEPENDPATH += $$PWD/../ChatCpp-Commands
