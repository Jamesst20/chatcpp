#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QTcpSocket>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>

class Utils{
public:
    static QString getServerList(){
        QEventLoop eventLoop;

        // "quit()" the event-loop, when the network request "finished()"
        QNetworkAccessManager mgr;
        QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

        //Http request
        QNetworkRequest req(QUrl("http://chatcppapi.somee.com/IPs.txt"));
        QNetworkReply *reply = mgr.get(req);

        //Wait until slot finished has been called.
        eventLoop.exec();

        QString replyStr = reply->readAll();

        delete reply;

        return replyStr;

    }
};

#endif // UTILS_H
