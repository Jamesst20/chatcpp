#ifndef GUI_LOGIN_H
#define GUI_LOGIN_H

#include <QMainWindow>
#include <QObject>
#include <QLabel>
#include <QDebug>

#include "GUI_Chat.h"
#include "ClientServer.h"
#include "ClientController.h"
#include <DialogServerList.h>

namespace Ui {
    class GUI_Login;
}

class GUI_Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit GUI_Login();
    ~GUI_Login();

private slots:
    void on_bt_connect_clicked();
    void connectedToServer();
    void disconnectedFromServer();
    void serverError(QString);
    void connectionAccepted();
    void connectionRefused(QString);

    void on_bt_servers_list_clicked();

private:
    Ui::GUI_Login *ui;
    GUI_Chat *gui_chat;
    ClientServer *clientServer;
    ClientController *clientController;
    QLabel *lbl_status;
};

#endif // GUI_LOGIN_H
