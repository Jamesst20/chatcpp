#ifndef COMMANDCONNECTIONACCEPT_H
#define COMMANDCONNECTIONACCEPT_H

#define COMMAND_CONNECTION_ACCEPT "CONNECTIONACCEPT"

#include "Command.hpp"

class CommandConnectionAccept : public Command{

public:
    explicit CommandConnectionAccept(): Command(COMMAND_CONNECTION_ACCEPT){}

    virtual QString getFormattedCommand(){
        return this->prefix;
    }

};

#endif // COMMANDCONNECTIONACCEPT_H
