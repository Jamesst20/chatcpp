#ifndef COMMANDYOUTUBEMP3_H
#define COMMANDYOUTUBEMP3_H

#define COMMAND_YOUTUBE_MP3 "YOUTUBE_MP3"

#include <QStringList>

#include "Command.hpp"

class CommandYoutubeMP3 : public Command {

public:
    CommandYoutubeMP3(QString url) : Command(COMMAND_YOUTUBE_MP3){this->url = url;}

    virtual QString getFormattedCommand(){
        return this->prefix + ";" + this->url;
    }

    QString getYoutubeURL(){
        return this->url;
    }

    static CommandYoutubeMP3 *parseCommand(QString formattedCommand){
        if(!formattedCommand.startsWith(COMMAND_YOUTUBE_MP3)){
            return 0;
        }
        QStringList split = formattedCommand.split(";");

        if(split.size() < 2){
            return 0;
        }

        return new CommandYoutubeMP3(split.at(1));
    }

private:
    QString url;

};

#endif // COMMANDYOUTUBEMP3_H
