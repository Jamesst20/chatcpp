#ifndef COMMANDGUSERSLIST_H
#define COMMANDGUSERSLIST_H

#define COMMAND_GUSERS_LIST "GUSERS_LIST"

#include "Command.hpp"

class CommandGUsersList : public Command {

public:
    CommandGUsersList() : Command(COMMAND_GUSERS_LIST){}

    virtual QString getFormattedCommand(){
        return this->prefix;
    }

};

#endif // COMMANDGUSERSLIST_H
