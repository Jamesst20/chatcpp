#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QHostInfo>
#include <QTcpSocket>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>

class Utils{

public:
    static const int port = 2048;

    static QString getLocalIP(){
        QTcpSocket googleDnsSocket;
        googleDnsSocket.connectToHost("8.8.8.8", 53);
        if (googleDnsSocket.waitForConnected(3000))
        {
            QString localIP = googleDnsSocket.localAddress().toString();
            return localIP;
        }
        return "0.0.0.0";
    }

    static QString getExternalIP(){
            QEventLoop eventLoop;

            // "quit()" the event-loop, when the network request "finished()"
            QNetworkAccessManager mgr;
            QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

            //Http request
            QNetworkRequest req(QUrl("http://myexternalip.com/raw"));
            QNetworkReply *reply = mgr.get(req);

            //Wait until slot finished has been called.
            eventLoop.exec();

            QString replyStr = reply->readAll();
            //Remove white spaces
            replyStr = replyStr.simplified();
            replyStr.replace(" ", "");

            delete reply;

            return replyStr;
    }

    static void registerServerIP(){
            QEventLoop eventLoop;

            // "quit()" the event-loop, when the network request "finished()"
            QNetworkAccessManager mgr;
            QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

            //Http request
            QNetworkRequest req(QUrl("http://chatcppapi.somee.com?task=add&localIP=" + getLocalIP() + "&externalIP=" + getExternalIP() + "&port=" + QString::number(port) + "&pcName=" + QHostInfo::localHostName()));
            QNetworkReply *reply = mgr.get(req);

            //Wait until slot finished has been called.
            eventLoop.exec();

         /*
            if (reply->error() == QNetworkReply::NoError) {
                qDebug() << "Success : " << reply->readAll();
            }
            else {
                qDebug() << "Failure : " << reply->errorString();

            }
        */
            delete reply;
    }

    static void unregisterServerIP(){
            QEventLoop eventLoop;

            // "quit()" the event-loop, when the network request "finished()"
            QNetworkAccessManager mgr;
            QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

            //Http request
            QNetworkRequest req(QUrl("http://chatcppapi.somee.com?task=delete&localIP=" + getLocalIP() + "&externalIP=" + getExternalIP() + "&port=" + QString::number(port) + "&pcName=" + QHostInfo::localHostName()));
            QNetworkReply *reply = mgr.get(req);

            //Wait until slot finished has been called.
            eventLoop.exec();

         /*
            if (reply->error() == QNetworkReply::NoError) {
                qDebug() << "Success : " << reply->readAll();
            }
            else {
                qDebug() << "Failure : " << reply->errorString();

            }
        */
            delete reply;
    }
};

#endif // UTILS_H
