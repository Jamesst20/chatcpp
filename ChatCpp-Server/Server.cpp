#include "Server.h"


Server::Server(qint16 port)
{
    this->server = new QTcpServer();
    this->clients = new QList<Client*>();

    if(!this->server->listen(QHostAddress::Any, port)){
        qDebug() << "Failed to run server : " << this->server->errorString();
    }else{
        //Register server on API
        qDebug() << "Registering server on API...";
        Utils::registerServerIP();
        qDebug() << "Server listening on port : " << port;
    }

    //Connect socket signals
    connect(this->server, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(acceptError(QAbstractSocket::SocketError)));
    connect(this->server, SIGNAL(newConnection()), this, SLOT(newConnection()));

}

Server::~Server(){
    delete this->server;
    delete this->clients;
}

QList<Client*> Server::getClientsList(){
    return *(this->clients);
}

void Server::newConnection(){
    //Create new client
    Client *newClient = new Client(this->server->nextPendingConnection());

    //Connect client signals
    connect(newClient, SIGNAL(dataReceived(Client*,QString)), this, SIGNAL(receivedDataFromClient(Client*,QString)));
    connect(newClient, SIGNAL(clientDisconnected(Client*)), this, SLOT(lostConnection(Client*)));

    //Add to client list
    clients->append(newClient);

    emit clientConnected(newClient);
    qDebug() << "A client has connected";
}

void Server::lostConnection(Client *client){
    clients->removeOne(client);
    emit clientDisconnected(client);
    qDebug() << "A client has disconnected.";
}

Client *Server::getClientForUsername(QString username){
    for(int i = 0; i < this->clients->size(); i++){
        if(this->clients->at(i)->getClientInfo()){
            if(this->clients->at(i)->getClientInfo()->getUsername() == username){
                return this->clients->at(i);
            }
        }
    }
    return 0;
}

void Server::broadcastCommand(Command *command){
    for(int i = 0; i < this->clients->size(); i++){
        this->clients->at(i)->sendData(command);
    }
}

void Server::acceptError(QAbstractSocket::SocketError){
    qDebug() << "acceptError : " << this->server->errorString();
}
