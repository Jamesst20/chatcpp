#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QString>
#include <QtGlobal>

#include "Client.h"
#include "Utils.h"

class Server : public QObject
{
    Q_OBJECT
public:
    Server(qint16 port);
    ~Server();
    QList<Client*> getClientsList();
    Client *getClientForUsername(QString username);
    void broadcastCommand(Command *command);

signals:
    void receivedDataFromClient(Client *, QString data);
    void clientConnected(Client *);
    void clientDisconnected(Client *);

private slots:
    void newConnection();
    void acceptError(QAbstractSocket::SocketError error);
    void lostConnection(Client *);

private:
    QTcpServer *server;
    QList<Client*> *clients;
};

#endif // SERVER_H
