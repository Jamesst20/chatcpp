#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H

#include <QStringList>

#include "Server.h"
#include "Client.h"

#include "Commands.hpp"


class ServerController : public QObject
{
    Q_OBJECT
public:
    ServerController(Server *server);
    bool toggleMuteUser(Client *client);

signals:
    void userAuthenticated(QString username);
    void userDisconnected(QString username);
    void onMessageCommand(CommandMessage *);

private slots:
    void receivedDataFromClient(Client *, QString data);
    void clientConnected(Client *client);
    void clientDisconnected(Client *client);

private:
    Server *server;
    QStringList mutedUsers;
    void sendCommandToAllClients(Command *command);
};

#endif // SERVERCONTROLLER_H
