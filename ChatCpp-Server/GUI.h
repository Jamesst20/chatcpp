#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include <QInputDialog>
#include <QMessageBox>

#include "Q_DebugStream.h"
#include "Server.h"
#include "ServerController.h"
#include "Utils.h"

#include "CommandMessage.hpp"


namespace Ui {
class GUI;
}

class GUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit GUI();
    ~GUI();

private slots:
    void on_txt_console_command_returnPressed();
    void onUserAuthenticated(QString username);
    void onUserDisconnected(QString username);
    void onMessageCommand(CommandMessage *);

    void on_bt_disconnect_client_clicked();
    void on_bt_merrychristmas_client_clicked();
    void on_bt_play_youtube_client_clicked();
    void on_bt_send_msg_to_client_clicked();
    void on_bt_send_as_client_clicked();
    void on_bt_mute_client_clicked();
    void on_txt_send_message_chat_returnPressed();

private:
    Ui::GUI *ui;
    Server *server;
    ServerController *serverController;
};

#endif // GUI_H
